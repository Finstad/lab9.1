package com.example.havresekken.lab91;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class MyService extends IntentService {
    public boolean serviceStopped = false;

    public MyService() {
        super("MyService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Toast.makeText(getApplicationContext(), "Service started", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Toast.makeText(getApplicationContext(), "Service stopped", Toast.LENGTH_LONG).show();
        serviceStopped = true;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        HttpURLConnection c = null;

        while(!serviceStopped) {
            try {
                URL u = new URL("https://kark.uit.no/~wfa004/weather/vdata.php");
                c = (HttpURLConnection) u.openConnection();
                c.connect();

                BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                String line = br.readLine();

                br.close();

                Intent broadCastIntent = new Intent();

                broadCastIntent.setAction(MainActivity.BROADCAST_ACTION);
                broadCastIntent.putExtra("data", line);

                sendBroadcast(broadCastIntent);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
