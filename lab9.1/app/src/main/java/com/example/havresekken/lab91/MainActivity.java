package com.example.havresekken.lab91;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    MyBroadCastReceiver myBroadCastReceiver;
    public static final String BROADCAST_ACTION = "ReceiveData";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void startService () {
        Intent myIntent = new Intent(this, MyService.class);
        startService(myIntent);
        myBroadCastReceiver = new MyBroadCastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BROADCAST_ACTION);
        registerReceiver(myBroadCastReceiver, intentFilter);
    }

    public void stopService () {
        Intent myIntent = new Intent(this, MyService.class);

        stopService(myIntent);
    }

    public void buttonClick (View view) {
        if(view.getId() == R.id.start){
            startService();
        }

        if(view.getId() == R.id.stop){
            stopService();
        }
    }

    class MyBroadCastReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

            try
            {
                String data = intent.getStringExtra("data");

                JSONObject jsonObject = new JSONObject(data);

                TextView timestamp = findViewById(R.id.timestamp);
                TextView temperature = findViewById(R.id.temperature);
                TextView pressure = findViewById(R.id.pressure);
                TextView humidity = findViewById(R.id.humidity);

                timestamp.setText("Timestamp: " + jsonObject.get("timestamp").toString());
                temperature.setText("Temperature: " + jsonObject.get("temperature").toString());
                pressure.setText("Pressure: " + jsonObject.get("pressure").toString());
                humidity.setText("Humidity: " +jsonObject.get("humidity").toString());

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(myBroadCastReceiver);
        stopService();
    }
}


